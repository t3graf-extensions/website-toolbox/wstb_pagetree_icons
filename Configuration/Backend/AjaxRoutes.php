<?php

/*
 * This file is part of the package t3graf/wstb_pagetree_icons.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

return [
    'page_tree_icons_change_page_icon' => [
        'path' => '/pagetreeiconstool/changepageicon',
        'target' => \T3graf\WstbPagetreeIcons\Controller\PagetreeIconsToolController::class . '::changepageiconAction',
    ],
];
