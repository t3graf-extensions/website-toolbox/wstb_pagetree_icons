<?php

/*
 * This file is part of the package t3graf/wstb_pagetree_icons.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3_MODE') or die();

/* * *************************************************************
 *  Copyright notice
 *
 *  (c) 2021 Regis TEDONE <regis.tedone@gmail.com>, SYRADEV
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *  A copy is found in the textfile GPL.txt and important notices to the license
 *  from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 * ************************************************************* */

//
// Override Page Tree Icons
//

//use SYRADEV\RtPagesTreeIcons\Utility\RtBackendUtility;

$langFile = 'LLL:EXT:wstb_pagetree_icons/Resources/Private/Language/locallang.xlf:';

$pageIcons = [

    [$langFile . 'folderShop',                     'shop',                 'apps-pagetree-folder-contains-shop',               'svg', 'default'],
    [$langFile . 'folderApprove',                  'approve',              'apps-pagetree-folder-contains-approve',            'svg', 'default'],
    [$langFile . 'folderBoard',                    'board',                'apps-pagetree-folder-contains-board',              'svg', 'default'],
    [$langFile . 'folderNews',                    'news',                'apps-pagetree-folder-contains-news',              'svg', 'default'],
    [$langFile . 'page404',               '404error',                'apps-pagetree-page-404-error',                         'svg', 'page'],
    [$langFile . 'pageArchive',               'archivepage',              'apps-pagetree-page-archive',                       'svg', 'page'],
    [$langFile . 'pageArticle',               'articlepage',              'apps-pagetree-page-article',                       'svg', 'page'],
    [$langFile . 'pageCamera',               'Camerapage',              'apps-pagetree-page-camera',                       'svg', 'page'],
    [$langFile . 'pageDownload',               'downloadpage',              'apps-pagetree-page-download',                       'svg', 'page'],
    [$langFile . 'pageMegamenu',               'megamenupage',              'apps-pagetree-page-dropdown-menu',                       'svg', 'page'],
    [$langFile . 'pageEvent',               'eventpage',              'apps-pagetree-page-event',                       'svg', 'page'],
    [$langFile . 'pageHome',               'Homepage',              'apps-pagetree-page-home',                       'svg', 'page'],
    [$langFile . 'pageLanding',               'landingpage',              'apps-pagetree-page-landing',                       'svg', 'page'],
    [$langFile . 'pageLegal',               'legalpage',              'apps-pagetree-page-legal',                       'svg', 'page'],
    [$langFile . 'pagePhotogallery',               'photogallerypage',              'apps-pagetree-page-photo-gallery',                       'svg', 'page'],
    [$langFile . 'pageSitemap',               'sitemappage',              'apps-pagetree-page-sitemap',                       'svg', 'page'],
    [$langFile . 'pageTestimonials',               'testimonialspage',              'apps-pagetree-page-testimonials',                       'svg', 'page'],

    /*
[$langFile . 'pageb',               'pageb-h',              'apps-pagetree-page-black-h',                       'svg','page'],
[$langFile . 'pagem',               'pagem',                'apps-pagetree-page-brown',                         'svg','page'],
[$langFile . 'pagem',               'pagem-s',              'apps-pagetree-page-brown-s',                       'svg','page'],
[$langFile . 'pagem',               'pagem-h',              'apps-pagetree-page-brown-h',                       'svg','page'],
[$langFile . 'pageblt',             'pageblt',              'apps-pagetree-page-blue-light',                    'svg','page'],
[$langFile . 'pageblt',             'pageblt-s',            'apps-pagetree-page-blue-light-s',                  'svg','page'],
[$langFile . 'pageblt',             'pageblt-h',            'apps-pagetree-page-blue-light-h',                  'svg','page'],
[$langFile . 'pagebl',              'pagebl',               'apps-pagetree-page-blue',                          'svg','page'],
[$langFile . 'pagebl',              'pagebl-s',             'apps-pagetree-page-blue-s',                        'svg','page'],
[$langFile . 'pagebl',              'pagebl-h',             'apps-pagetree-page-blue-h',                        'svg','page'],
[$langFile . 'pageblf',             'pageblf',              'apps-pagetree-page-blue-neon',                     'svg','page'],
[$langFile . 'pageblf',             'pageblf-s',            'apps-pagetree-page-blue-neon-s',                   'svg','page'],
[$langFile . 'pageblf',             'pageblf-h',            'apps-pagetree-page-blue-neon-h',                   'svg','page'],
[$langFile . 'pagegb',              'pagegb',               'apps-pagetree-page-gray-brighter',                 'svg','page'],
[$langFile . 'pagegb',              'pagegb-s',             'apps-pagetree-page-gray-brighter-s',               'svg','page'],
[$langFile . 'pagegb',              'pagegb-h',             'apps-pagetree-page-gray-brighter-h',               'svg','page'],
[$langFile . 'pagegd',              'pagegd',               'apps-pagetree-page-gray-dark',                     'svg','page'],
[$langFile . 'pagegd',              'pagegd-s',             'apps-pagetree-page-gray-dark-s',                   'svg','page'],
[$langFile . 'pagegd',              'pagegd-h',             'apps-pagetree-page-gray-dark-h',                   'svg','page'],
[$langFile . 'pagegl',              'pagegl',               'apps-pagetree-page-gray-light',                    'svg','page'],
[$langFile . 'pagegl',              'pagegl-s',             'apps-pagetree-page-gray-light-s',                  'svg','page'],
[$langFile . 'pagegl',              'pagegl-h',             'apps-pagetree-page-gray-light-h',                  'svg','page'],
[$langFile . 'pageg',               'pageg',                'apps-pagetree-page-gray',                          'svg','page'],
[$langFile . 'pageg',               'pageg-s',              'apps-pagetree-page-gray-s',                        'svg','page'],
[$langFile . 'pageg',               'pageg-h',              'apps-pagetree-page-gray-h',                        'svg','page'],
[$langFile . 'pagegrl',             'pagegrl',              'apps-pagetree-page-green-light',                   'svg','page'],
[$langFile . 'pagegrl',             'pagegrl-s',            'apps-pagetree-page-green-light-s',                 'svg','page'],
[$langFile . 'pagegrl',             'pagegrl-h',            'apps-pagetree-page-green-light-h',                 'svg','page'],
[$langFile . 'pagegr',              'pagegr',               'apps-pagetree-page-green',                         'svg','page'],
[$langFile . 'pagegr',              'pagegr-s',             'apps-pagetree-page-green-s',                       'svg','page'],
[$langFile . 'pagegr',              'pagegr-h',             'apps-pagetree-page-green-h',                       'svg','page'],
[$langFile . 'pagegrf',             'pagegrf',              'apps-pagetree-page-green-neon',                    'svg','page'],
[$langFile . 'pagegrf',             'pagegrf-s',            'apps-pagetree-page-green-neon-s',                  'svg','page'],
[$langFile . 'pagegrf',             'pagegrf-h',            'apps-pagetree-page-green-neon-h',                  'svg','page'],
[$langFile . 'pageo',               'pageo',                'apps-pagetree-page-orange',                        'svg','page'],
[$langFile . 'pageo',               'pageo-s',              'apps-pagetree-page-orange-s',                      'svg','page'],
[$langFile . 'pageo',               'pageo-h',              'apps-pagetree-page-orange-h',                      'svg','page'],
[$langFile . 'pageof',              'pageof',               'apps-pagetree-page-orange-neon',                   'svg','page'],
[$langFile . 'pageof',              'pageof-s',             'apps-pagetree-page-orange-neon-s',                 'svg','page'],
[$langFile . 'pageof',              'pageof-h',             'apps-pagetree-page-orange-neon-h',                 'svg','page'],
[$langFile . 'pagep',               'pagep',                'apps-pagetree-page-purple',                        'svg','page'],
[$langFile . 'pagep',               'pagep-s',              'apps-pagetree-page-purple-s',                      'svg','page'],
[$langFile . 'pagep',               'pagep-h',              'apps-pagetree-page-purple-h',                      'svg','page'],
[$langFile . 'pagef',               'pagef',                'apps-pagetree-page-fuchsia',                       'svg','page'],
[$langFile . 'pagef',               'pagef-s',              'apps-pagetree-page-fuchsia-s',                     'svg','page'],
[$langFile . 'pagef',               'pagef-h',              'apps-pagetree-page-fuchsia-h',                     'svg','page'],
[$langFile . 'pagerl',              'pagerl',               'apps-pagetree-page-red-light',                     'svg','page'],
[$langFile . 'pagerl',              'pagerl-s',             'apps-pagetree-page-red-light-s',                   'svg','page'],
[$langFile . 'pagerl',              'pagerl-h',             'apps-pagetree-page-red-light-h',                   'svg','page'],
[$langFile . 'pager',               'pager',                'apps-pagetree-page-red',                           'svg','page'],
[$langFile . 'pager',               'pager-s',              'apps-pagetree-page-red-s',                         'svg','page'],
[$langFile . 'pager',               'pager-h',              'apps-pagetree-page-red-h',                         'svg','page'],
[$langFile . 'pagerf',              'pagerf',               'apps-pagetree-page-red-neon',                      'svg','page'],
[$langFile . 'pagerf',              'pagerf-s',             'apps-pagetree-page-red-neon-s',                    'svg','page'],
[$langFile . 'pagerf',              'pagerf-h',             'apps-pagetree-page-red-neon-h',                    'svg','page'],
[$langFile . 'pagew',               'pagew',                'apps-pagetree-page-white',                         'svg','page'],
[$langFile . 'pagew',               'pagew-s',              'apps-pagetree-page-white-s',                       'svg','page'],
[$langFile . 'pagew',               'pagew-h',              'apps-pagetree-page-white-h',                       'svg','page'],
[$langFile . 'pageyl',              'pageyl',               'apps-pagetree-page-yellow-light',                  'svg','page'],
[$langFile . 'pageyl',              'pageyl-s',             'apps-pagetree-page-yellow-light-s',                'svg','page'],
[$langFile . 'pageyl',              'pageyl-h',             'apps-pagetree-page-yellow-light-h',                'svg','page'],
[$langFile . 'pagey',               'pagey',                'apps-pagetree-page-yellow',                        'svg','page'],
[$langFile . 'pagey',               'pagey-s',              'apps-pagetree-page-yellow-s',                      'svg','page'],
[$langFile . 'pagey',               'pagey-h',              'apps-pagetree-page-yellow-h',                      'svg','page'],
[$langFile . 'pageyf',              'pageyf',               'apps-pagetree-page-yellow-neon',                   'svg','page'],
[$langFile . 'pageyf',              'pageyf-s',             'apps-pagetree-page-yellow-neon-s',                 'svg','page'],
[$langFile . 'pageyf',              'pageyf-h',             'apps-pagetree-page-yellow-neon-h',                 'svg','page'],
[$langFile . 'pagegdpr',            'pagegdpr',             'apps-pagetree-page-gdpr',                          'svg','page'],
[$langFile . 'pagegdpr',            'pagegdpr-s',           'apps-pagetree-page-gdpr-s',                        'svg','page'],
[$langFile . 'pagegdpr',            'pagegdpr-h',           'apps-pagetree-page-gdpr-h',                        'svg','page'],
*/

    [$langFile . 'folderb',             'folderb',              'apps-pagetree-folder-black',              'svg', 'folder'],
    [$langFile . 'folderblt',           'folderblt',            'apps-pagetree-folder-blue-light',         'svg', 'folder'],
    [$langFile . 'folderbl',            'folderbl',             'apps-pagetree-folder-blue',               'svg', 'folder'],
    [$langFile . 'folderblf',           'folderblf',            'apps-pagetree-folder-blue-neon',          'svg', 'folder'],
    [$langFile . 'foldergd',            'foldergd',             'apps-pagetree-folder-gray-dark',          'svg', 'folder'],
    [$langFile . 'foldergl',            'foldergl',             'apps-pagetree-folder-gray-light',         'svg', 'folder'],
    [$langFile . 'folderg',             'folderg',              'apps-pagetree-folder-gray',               'svg', 'folder'],
    [$langFile . 'foldergrl',           'foldergrl',            'apps-pagetree-folder-green-light',        'svg', 'folder'],
    [$langFile . 'foldergr',            'foldergr',             'apps-pagetree-folder-green',              'svg', 'folder'],
    [$langFile . 'foldergrf',           'foldergrf',            'apps-pagetree-folder-green-neon',         'svg', 'folder'],
    [$langFile . 'foldero',             'foldero',              'apps-pagetree-folder-orange',             'svg', 'folder'],
    [$langFile . 'folderof',            'folderof',             'apps-pagetree-folder-orange-neon',        'svg', 'folder'],
    [$langFile . 'folderf',             'folderf',              'apps-pagetree-folder-fuchsia',            'svg', 'folder'],
    [$langFile . 'folderrl',            'folderrl',             'apps-pagetree-folder-red-light',          'svg', 'folder'],
    [$langFile . 'folderr',             'folderr',              'apps-pagetree-folder-red',                'svg', 'folder'],
    [$langFile . 'folderrf',            'folderrf',             'apps-pagetree-folder-red-neon',           'svg', 'folder'],
    [$langFile . 'folderyl',            'folderyl',             'apps-pagetree-folder-yellow-light',       'svg', 'folder'],
    [$langFile . 'foldery',             'foldery',              'apps-pagetree-folder-yellow',             'svg', 'folder'],
    [$langFile . 'folderyf',            'folderyf',             'apps-pagetree-folder-yellow-neon',        'svg', 'folder'],
    [$langFile . 'foldermegamenu',            'foldermegamenu',             'apps-pagetree-folder-megamenu',        'svg', 'folder'],
    [$langFile . 'datablack',            'datablack',             'apps-pagetree-data-black',        'svg', 'data'],
    [$langFile . 'datablue',            'datablue',             'apps-pagetree-data-blue',        'svg', 'data'],
    [$langFile . 'databluel',            'databluel',             'apps-pagetree-data-blue-light',        'svg', 'data'],
    [$langFile . 'databluen',            'databluen',             'apps-pagetree-data-blue-neon',        'svg', 'data'],
    [$langFile . 'datafuchsia',            'datafuchsia',             'apps-pagetree-data-fuchsia',        'svg', 'data'],
    [$langFile . 'datagray',            'datagray',             'apps-pagetree-data-gray',        'svg', 'data'],
    [$langFile . 'datagrayd',            'datagrayd',             'apps-pagetree-data-gray-dark',        'svg', 'data'],
    [$langFile . 'datagrayl',            'datagrayl',             'apps-pagetree-data-gray-light',        'svg', 'data'],
    [$langFile . 'datagreen',            'datagreen',             'apps-pagetree-data-green',        'svg', 'data'],
    [$langFile . 'datagreenl',            'datagreenl',             'apps-pagetree-data-green-light',        'svg', 'data'],
    [$langFile . 'datagreenn',            'datagreenn',             'apps-pagetree-data-green-neon',        'svg', 'data'],
    [$langFile . 'dataorange',            'dataorange',             'apps-pagetree-data-orange',        'svg', 'data'],
    [$langFile . 'dataorangen',            'dataorangen',             'apps-pagetree-data-orange-neon',        'svg', 'data'],
    [$langFile . 'datared',            'datared',             'apps-pagetree-data-red',        'svg', 'data'],
    [$langFile . 'dataredl',            'dataredl',             'apps-pagetree-data-red-light',        'svg', 'data'],
    [$langFile . 'dataredn',            'dataredn',             'apps-pagetree-data-red-neon',        'svg', 'data'],
    [$langFile . 'datayellow',            'datayellow',             'apps-pagetree-data-yellow',        'svg', 'data'],
    [$langFile . 'datayellowl',            'datayellowl',             'apps-pagetree-data-yellow-light',        'svg', 'data'],
    [$langFile . 'datayellown',            'datayellown',             'apps-pagetree-data-yellow-neon',        'svg', 'data'],
    [$langFile . 'dataarticle',            'dataarticle',             'apps-pagetree-data-article',        'svg', 'data'],
    [$langFile . 'dataevent',            'dataevent',             'apps-pagetree-data-event',        'svg', 'data'],
/*
    [$langFile . 'symbcoconut',         'symbcocotier',         'actions-pagetree-change-page-icon',                'svg','symbol'],
    [$langFile . 'symbcoconut',         'symbcocotier-h',       'actions-pagetree-change-page-icon-h',              'svg','symbol'],
    [$langFile . 'symbshortcut',        'symbshortcut',         'apps-pagetree-shortcut',                           'svg','symbol'],
    [$langFile . 'symbshortcut',        'symbshortcut-h',       'apps-pagetree-shortcut-h',                         'svg','symbol'],
    [$langFile . 'symblandpage',        'symblandpage',         'apps-pagetree-landing-page',                       'svg','symbol'],
    [$langFile . 'symblandpage',        'symblandpage-h',       'apps-pagetree-landing-page-h',                     'svg','symbol'],
    [$langFile . 'apppresentation',     'apppresentation',      'apps-pagetree-presentation',                       'svg','symbol'],
    [$langFile . 'apppresentation',     'apppresentation-h',    'apps-pagetree-presentation-h',                     'svg','symbol'],
    [$langFile . 'symbdownload',        'symbdownload',         'apps-pagetree-download',                           'svg','symbol'],
    [$langFile . 'symbdownload',        'symbdownload-h',       'apps-pagetree-download-h',                         'svg','symbol'],
    [$langFile . 'symbform',            'symbform',             'apps-pagetree-form',                               'svg','symbol'],
    [$langFile . 'symbform',            'symbform-h',           'apps-pagetree-form-h',                             'svg','symbol'],
    [$langFile . 'symbdesktop',         'symbdesktop',          'apps-pagetree-ux-desktop',                         'svg','symbol'],
    [$langFile . 'symbdesktop',         'symbdesktop-h',        'apps-pagetree-ux-desktop-h',                       'svg','symbol'],
    [$langFile . 'symbtablet',          'symbtablet',           'apps-pagetree-ux-tablet',                          'svg','symbol'],
    [$langFile . 'symbtablet',          'symbtablet-h',         'apps-pagetree-ux-tablet-h',                        'svg','symbol'],
    [$langFile . 'symbmobile',          'symbmobile',           'apps-pagetree-ux-mobile',                          'svg','symbol'],
    [$langFile . 'symbmobile',          'symbmobile-h',         'apps-pagetree-ux-mobile-h',                        'svg','symbol'],
    [$langFile . 'symbhome',            'symbhome',             'apps-pagetree-home',                               'svg','symbol'],
    [$langFile . 'symbhome',            'symbhome-h',           'apps-pagetree-home-h',                             'svg','symbol'],
    [$langFile . 'symbvideo',           'symbvideo',            'apps-pagetree-video',                              'svg','symbol'],
    [$langFile . 'symbvideo',           'symbvideo-h',          'apps-pagetree-video-h',                            'svg','symbol'],
    [$langFile . 'symbaudio',           'symbaudio',            'apps-pagetree-audio',                              'svg','symbol'],
    [$langFile . 'symbaudio',           'symbaudio-h',          'apps-pagetree-audio-h',                            'svg','symbol'],
    [$langFile . 'symbcamera',          'symbcamera',           'apps-pagetree-camera',                             'svg','symbol'],
    [$langFile . 'symbcamera',          'symbcamera-h',         'apps-pagetree-camera-h',                           'svg','symbol'],
    [$langFile . 'symbimage',           'symbcimage',           'apps-pagetree-image',                              'svg','symbol'],
    [$langFile . 'symbimage',           'symbcimage-h',         'apps-pagetree-image-h',                            'svg','symbol'],
    [$langFile . 'symbcomments',        'symbcomments',         'apps-pagetree-comments',                           'svg','symbol'],
    [$langFile . 'symbcomments',        'symbcomments-h',       'apps-pagetree-comments-h',                         'svg','symbol'],
    [$langFile . 'symbmagnifier',       'symbmagnifier',        'apps-pagetree-magnifier',                          'svg','symbol'],
    [$langFile . 'symbmagnifier',       'symbmagnifier-h',      'apps-pagetree-magnifier-h',                        'svg','symbol'],
    [$langFile . 'symbmail',            'symbmail',             'apps-pagetree-mail',                               'svg','symbol'],
    [$langFile . 'symbmail',            'symbmail-h',           'apps-pagetree-mail-h',                             'svg','symbol'],
    [$langFile . 'symbbooks',           'symbbooks',            'apps-pagetree-books',                              'svg','symbol'],
    [$langFile . 'symbbooks',           'symbbooks-h',          'apps-pagetree-books-h',                            'svg','symbol'],
    [$langFile . 'symbjobs',            'symbjobs',             'apps-pagetree-jobs',                               'svg','symbol'],
    [$langFile . 'symbjobs',            'symbjobs-h',           'apps-pagetree-jobs-h',                             'svg','symbol'],
    [$langFile . 'symblegalnotice',     'symblegalnotice',      'apps-pagetree-legal-notice',                       'svg','symbol'],
    [$langFile . 'symblegalnotice',     'symblegalnotice-h',    'apps-pagetree-legal-notice-h',                     'svg','symbol'],
    [$langFile . 'symbsitemap',         'symbsitemap',          'apps-pagetree-sitemap',                            'svg','symbol'],
    [$langFile . 'symbsitemap',         'symbsitemap-h',        'apps-pagetree-sitemap-h',                          'svg','symbol'],
    [$langFile . 'symbmenu',            'symbmenu',             'apps-pagetree-menu',                               'svg','symbol'],
    [$langFile . 'symbmenu',            'symbmenu-h',           'apps-pagetree-menu-h',                             'svg','symbol'],
    [$langFile . 'symbagenda',          'symbagenda',           'apps-pagetree-agenda',                             'svg','symbol'],
    [$langFile . 'symbagenda',          'symbagenda-h',         'apps-pagetree-agenda-h',                           'svg','symbol'],
    [$langFile . 'symbphone',           'symbphone',            'apps-pagetree-phone',                              'svg','symbol'],
    [$langFile . 'symbphone',           'symbphone-h',          'apps-pagetree-phone-h',                            'svg','symbol'],
    [$langFile . 'symblocation',        'symblocation',         'apps-pagetree-location',                           'svg','symbol'],
    [$langFile . 'symblocation',        'symblocation-h',       'apps-pagetree-location-h',                         'svg','symbol'],
    [$langFile . 'symbcloud',           'symbcloud',            'apps-pagetree-cloud',                              'svg','symbol'],
    [$langFile . 'symbcloud',           'symbcloud-h',          'apps-pagetree-cloud-h',                            'svg','symbol'],
    [$langFile . 'symbmeteo',           'symbmeteo',            'apps-pagetree-meteo',                              'svg','symbol'],
    [$langFile . 'symbmeteo',           'symbmeteo-h',          'apps-pagetree-meteo-h',                            'svg','symbol'],
    [$langFile . 'symbcredcard',        'symbcredcard',         'apps-pagetree-credit-card',                        'svg','symbol'],
    [$langFile . 'symbcredcard',        'symbcredcard-h',       'apps-pagetree-credit-card-h',                      'svg','symbol'],
    [$langFile . 'symbshopcart',        'symbshopcart',         'apps-pagetree-shopping-cart',                      'svg','symbol'],
    [$langFile . 'symbshopcart',        'symbshopcart-h',       'apps-pagetree-shopping-cart-h',                    'svg','symbol'],
    [$langFile . 'symbuniversity',      'symbuniversity',       'apps-pagetree-university',                         'svg','symbol'],
    [$langFile . 'symbuniversity',      'symbuniversity-h',     'apps-pagetree-university-h',                       'svg','symbol'],
    [$langFile . 'symbbrainstorm',      'symbbrainstorm',       'apps-pagetree-brainstorm',                         'svg','symbol'],
    [$langFile . 'symbbrainstorm',      'symbbrainstorm-h',     'apps-pagetree-brainstorm-h',                       'svg','symbol'],
    [$langFile . 'symbfaq',             'symbfaq',              'apps-pagetree-faq',                                'svg','symbol'],
    [$langFile . 'symbfaq',             'symbfaq-h',            'apps-pagetree-faq-h',                              'svg','symbol'],
    [$langFile . 'symbtestimonial',     'symbtestimonial',      'apps-pagetree-testimonial',                        'svg','symbol'],
    [$langFile . 'symbtestimonial',     'symbtestimonial-h',    'apps-pagetree-testimonial-h',                      'svg','symbol'],
    [$langFile . 'symbannouncement',    'symbannouncement',     'apps-pagetree-announcement',                       'svg','symbol'],
    [$langFile . 'symbannouncement',    'symbannouncement-h',   'apps-pagetree-announcement-h',                     'svg','symbol'],
    [$langFile . 'symbchart',           'symbchart',            'apps-pagetree-chart',                              'svg','symbol'],
    [$langFile . 'symbchart',           'symbchart-h',          'apps-pagetree-chart-h',                            'svg','symbol'],
    [$langFile . 'symbarob',            'symbarob',             'apps-pagetree-arobase-black',                      'svg','symbol'],
    [$langFile . 'symbarob',            'symbarob-h',           'apps-pagetree-arobase-black-h',                    'svg','symbol'],
    [$langFile . 'symbaroo',            'symbaroo',             'apps-pagetree-arobase-orange',                     'svg','symbol'],
    [$langFile . 'symbaroo',            'symbaroo-h',           'apps-pagetree-arobase-orange-h',                   'svg','symbol'],
    [$langFile . 'symbastb',            'symbastb',             'apps-pagetree-asterisk-black',                     'svg','symbol'],
    [$langFile . 'symbastb',            'symbastb-h',           'apps-pagetree-asterisk-black-h',                   'svg','symbol'],
    [$langFile . 'symbasto',            'symbasto',             'apps-pagetree-asterisk-orange',                    'svg','symbol'],
    [$langFile . 'symbasto',            'symbasto-h',           'apps-pagetree-asterisk-orange-h',                  'svg','symbol'],
    [$langFile . 'symbhtagb',           'symbhtagb',            'apps-pagetree-hashtag-black',                      'svg','symbol'],
    [$langFile . 'symbhtagb',           'symbhtagb-h',          'apps-pagetree-hashtag-black-h',                    'svg','symbol'],
    [$langFile . 'symbhtago',           'symbhtago',            'apps-pagetree-hashtag-orange',                     'svg','symbol'],
    [$langFile . 'symbhtago',           'symbhtago-h',          'apps-pagetree-hashtag-orange-h',                   'svg','symbol'],
    [$langFile . 'symbgearb',           'symbgearb',            'apps-pagetree-gear-black',                         'svg','symbol'],
    [$langFile . 'symbgearb',           'symbgearb-h',          'apps-pagetree-gear-black-h',                       'svg','symbol'],
    [$langFile . 'symbgearo',           'symbgearo',            'apps-pagetree-gear-orange',                        'svg','symbol'],
    [$langFile . 'symbgearo',           'symbgearo-h',          'apps-pagetree-gear-orange-h',                      'svg','symbol'],
    [$langFile . 'symbstarb',           'symbstarb',            'apps-pagetree-star-black',                         'svg','symbol'],
    [$langFile . 'symbstarb',           'symbstarb-h',          'apps-pagetree-star-black-h',                       'svg','symbol'],
    [$langFile . 'symbstaro',           'symbstaro',            'apps-pagetree-star-orange',                        'svg','symbol'],
    [$langFile . 'symbstaro',           'symbstaro-h',          'apps-pagetree-star-orange-h',                      'svg','symbol'],
    [$langFile . 'symbwrenchb',         'symbwrenchb',          'apps-pagetree-wrench-black',                       'svg','symbol'],
    [$langFile . 'symbwrenchb',         'symbwrenchb-h',        'apps-pagetree-wrench-black-h',                     'svg','symbol'],
    [$langFile . 'symbwrencho',         'symbwrencho',          'apps-pagetree-wrench-orange',                      'svg','symbol'],
    [$langFile . 'symbwrencho',         'symbwrencho-h',        'apps-pagetree-wrench-orange-h',                    'svg','symbol'],
    [$langFile . 'symbcube',            'symbcube',             'apps-pagetree-cube',                               'svg','symbol'],
    [$langFile . 'symbcube',            'symbcube-h',           'apps-pagetree-cube-h',                             'svg','symbol'],
    [$langFile . 'symbproduct',         'symbproduct',          'apps-pagetree-product',                            'svg','symbol'],
    [$langFile . 'symbproduct',         'symbproduct-h',        'apps-pagetree-product-h',                          'svg','symbol'],
    [$langFile . 'symbdelivery',        'symbdelivery',         'apps-pagetree-delivery',                           'svg','symbol'],
    [$langFile . 'symbdelivery',        'symbdelivery-h',       'apps-pagetree-delivery-h',                         'svg','symbol'],
    [$langFile . 'symbgift',            'symbgift',             'apps-pagetree-gift',                               'svg','symbol'],
    [$langFile . 'symbgift',            'symbgift-h',           'apps-pagetree-gift-h',                             'svg','symbol'],
    [$langFile . 'symbheartb',          'symbheartb',           'apps-pagetree-heart-b',                            'svg','symbol'],
    [$langFile . 'symbheartb',          'symbheartb-h',         'apps-pagetree-heart-b-h',                          'svg','symbol'],
    [$langFile . 'symbheartr',          'symbheartr',           'apps-pagetree-heart-r',                            'svg','symbol'],
    [$langFile . 'symbheartr',          'symbheartr-h',         'apps-pagetree-heart-r-h',                          'svg','symbol'],
    [$langFile . 'symbarchive',         'symbarchive',          'apps-pagetree-archive',                            'svg','symbol'],
    [$langFile . 'symbarchive',         'symbarchive-h',        'apps-pagetree-archive-h',                          'svg','symbol'],
    [$langFile . 'symbart',             'symbart',              'apps-pagetree-art',                                'svg','symbol'],
    [$langFile . 'symbart',             'symbart-h',            'apps-pagetree-art-h',                              'svg','symbol'],
    [$langFile . 'symbdb',              'symbdb',               'apps-pagetree-database',                           'svg','symbol'],
    [$langFile . 'symbdb',              'symbdb-h',             'apps-pagetree-database-h',                         'svg','symbol'],
    [$langFile . 'symblightbulb',       'symblightbulb',        'apps-pagetree-lightbulb',                          'svg','symbol'],
    [$langFile . 'symblightbulb',       'symblightbulb-h',      'apps-pagetree-lightbulb-h',                        'svg','symbol'],
    [$langFile . 'symblock',            'symblock',             'apps-pagetree-lock',                               'svg','symbol'],
    [$langFile . 'symblock',            'symblock-h',           'apps-pagetree-lock-h',                             'svg','symbol'],
    [$langFile . 'symbsak',             'symbsak',              'apps-pagetree-swiss-army-knife',                   'svg','symbol'],
    [$langFile . 'symbsak',             'symbsak-h',            'apps-pagetree-swiss-army-knife-h',                 'svg','symbol'],
    [$langFile . 'symbusers',           'symbusers',            'apps-pagetree-users',                              'svg','symbol'],
    [$langFile . 'symbusers',           'symbusers-h',          'apps-pagetree-users-h',                            'svg','symbol'],
    [$langFile . 'symbuser',            'symbuser',             'apps-pagetree-user',                               'svg','symbol'],
    [$langFile . 'symbuser',            'symbuser-h',           'apps-pagetree-user-h',                             'svg','symbol'],
    [$langFile . 'symbearth',           'symbearth',            'apps-pagetree-earth',                              'svg','symbol'],
    [$langFile . 'symbearth',           'symbearth-h',          'apps-pagetree-earth-h',                            'svg','symbol'],
    [$langFile . 'symbearth-s',         'symbearth-s',          'apps-pagetree-earth-s',                            'svg','symbol'],
    [$langFile . 'symbbookmark',        'symbbookmark',         'apps-pagetree-bookmark',                           'svg','symbol'],
    [$langFile . 'symbbookmark',        'symbbookmark-h',       'apps-pagetree-bookmark-h',                         'svg','symbol'],
    [$langFile . 'symblink',            'symblink',             'apps-pagetree-link',                               'svg','symbol'],
    [$langFile . 'symblink',            'symblink-h',           'apps-pagetree-link-h',                             'svg','symbol'],
    [$langFile . 'symblogin',           'symblogin',            'apps-pagetree-login',                              'svg','symbol'],
    [$langFile . 'symblogin',           'symblogin-h',          'apps-pagetree-login-h',                            'svg','symbol'],
    [$langFile . 'symblive',            'symblive',             'apps-pagetree-live',                               'svg','symbol'],
    [$langFile . 'symblive',            'symblive-h',           'apps-pagetree-live-h',                             'svg','symbol'],
*/
];
//$diplayIconsInBehaviourTab = 0;

//$extConf = RtBackendUtility::getExtensionConfiguration('rt_pages_tree_icons');

//$diplayIconsInBehaviourTab = $extConf['diplayIconsInBehaviourTab'];
/*
if(version_compare(TYPO3_version, '9.0', '<')) {
    $GLOBALS['TCA']['pages']['columns']['module']['config']['showIconTable'] = $diplayIconsInBehaviourTab;
} elseif(version_compare(TYPO3_version, '9.0', '>=')) {
    $GLOBALS['TCA']['pages']['columns']['module']['config']['fieldWizard']['selectIcons']['disabled'] = !$diplayIconsInBehaviourTab;
}
*/

foreach ($pageIcons as $iconsParams) {
    $iP = substr($iconsParams[1], -2);
    if ($iP !== '-s' || $iP !=='-h') {
        // krexx($iconsParams);
        $GLOBALS['TCA']['pages']['columns']['module']['config']['items'][] = $iconsParams;
    }

    $GLOBALS['TCA']['pages']['ctrl']['typeicon_classes'][ 'contains-' . $iconsParams[1] ] = $iconsParams[2];
}
