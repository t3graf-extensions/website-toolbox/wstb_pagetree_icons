<?php

/*
 * This file is part of the package t3graf/wstb_pagetree_icons.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3') || die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('wstb_pagetree_icons', 'Configuration/TypoScript', 'Pagetree Icons');
