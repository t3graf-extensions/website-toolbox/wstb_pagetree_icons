<?php

/*
 * This file is part of the package t3graf/wstb_pagetree_icons.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

return [
    // Icon identifier
   /* 'mysvgicon' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:my_extension/Resources/Public/Icons/mysvg.svg',
    ],
   */
    'apps-pagetree-page-404-error' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Pages/apps-pagetree-page-404-error.svg',
    ],
    'apps-pagetree-page-archive' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Pages/apps-pagetree-page-archive.svg',
    ],
    'apps-pagetree-page-article' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Pages/apps-pagetree-page-article.svg',
    ],
    'apps-pagetree-page-camera' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Pages/apps-pagetree-page-camera.svg',
    ],
    'apps-pagetree-page-download' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Pages/apps-pagetree-page-download.svg',
    ],
    'apps-pagetree-page-dropdown-menu' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Pages/apps-pagetree-page-dropdown-menu.svg',
    ],
    'apps-pagetree-page-event' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Pages/apps-pagetree-page-event.svg',
    ],
    'apps-pagetree-page-home' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Pages/apps-pagetree-page-home.svg',
    ],
    'apps-pagetree-page-landing' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Pages/apps-pagetree-page-landing.svg',
    ],
    'apps-pagetree-page-legal' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Pages/apps-pagetree-page-legal.svg',
    ],
    'apps-pagetree-page-photo-gallery' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Pages/apps-pagetree-page-photo-gallery.svg',
    ],
    'apps-pagetree-page-root' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Pages/apps-pagetree-page-root.svg',
    ],
    'apps-pagetree-page-sitemap' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Pages/apps-pagetree-page-sitemap.svg',
    ],
    'apps-pagetree-page-testimonials' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Pages/apps-pagetree-page-testimonials.svg',
    ],
    'apps-pagetree-folder-black' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Folders/apps-pagetree-folder-black.svg',
    ],
    'apps-pagetree-folder-blue' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Folders/apps-pagetree-folder-blue.svg',
    ],
    'apps-pagetree-folder-blue-light' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Folders/apps-pagetree-folder-blue-light.svg',
    ],
    'apps-pagetree-folder-blue-neon' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Folders/apps-pagetree-folder-blue-neon.svg',
    ],
    'apps-pagetree-folder-gray-dark' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Folders/apps-pagetree-folder-gray-dark.svg',
    ],
    'apps-pagetree-folder-gray-light' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Folders/apps-pagetree-folder-gray-light.svg',
    ],
    'apps-pagetree-folder-gray' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Folders/apps-pagetree-folder-gray.svg',
    ],
    'apps-pagetree-folder-green-light' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Folders/apps-pagetree-folder-green-light.svg',
    ],
    'apps-pagetree-folder-green' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Folders/apps-pagetree-folder-green.svg',
    ],
    'apps-pagetree-folder-green-neon' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Folders/apps-pagetree-folder-green-neon.svg',
    ],
    'apps-pagetree-folder-orange' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Folders/apps-pagetree-folder-orange.svg',
    ],
    'apps-pagetree-folder-orange-neon' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Folders/apps-pagetree-folder-orange-neon.svg',
    ],
    'apps-pagetree-folder-fuchsia' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Folders/apps-pagetree-folder-fuchsia.svg',
    ],
    'apps-pagetree-folder-red-light' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Folders/apps-pagetree-folder-red-light.svg',
    ],
    'apps-pagetree-folder-red' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Folders/apps-pagetree-folder-red.svg',
    ],
    'apps-pagetree-folder-red-neon' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Folders/apps-pagetree-folder-red-neon.svg',
    ],
    'apps-pagetree-folder-yellow-light' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Folders/apps-pagetree-folder-yellow-light.svg',
    ],
    'apps-pagetree-folder-yellow' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Folders/apps-pagetree-folder-yellow.svg',
    ],
    'apps-pagetree-folder-yellow-neon' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Folders/apps-pagetree-folder-yellow-neon.svg',
    ],
    'apps-pagetree-folder-megamenu' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Folders/apps-pagetree-folder-megamenu.svg',
    ],
    'apps-pagetree-data-black' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Storages/apps-pagetree-data-black.svg',
    ],
    'apps-pagetree-data-blue' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Storages/apps-pagetree-data-blue.svg',
    ],
    'apps-pagetree-data-blue-light' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Storages/apps-pagetree-data-blue-light.svg',
    ],
    'apps-pagetree-data-blue-neon' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Storages/apps-pagetree-data-blue-neon.svg',
    ],
    'apps-pagetree-data-fuchsia' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Storages/apps-pagetree-data-fuchsia.svg',
    ],
    'apps-pagetree-data-gray' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Storages/apps-pagetree-data-gray.svg',
    ],
    'apps-pagetree-data-gray-dark' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Storages/apps-pagetree-data-gray-dark.svg',
    ],
    'apps-pagetree-data-gray-light' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Storages/apps-pagetree-data-gray-light.svg',
    ],
    'apps-pagetree-data-green' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Storages/apps-pagetree-data-green.svg',
    ],
    'apps-pagetree-data-green-light' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Storages/apps-pagetree-data-green-light.svg',
    ],
    'apps-pagetree-data-green-neon' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Storages/apps-pagetree-data-green-neon.svg',
    ],
    'apps-pagetree-data-orange' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Storages/apps-pagetree-data-orange.svg',
    ],
    'apps-pagetree-data-orange-neon' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Storages/apps-pagetree-data-orange-neon.svg',
    ],
    'apps-pagetree-data-red' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Storages/apps-pagetree-data-red.svg',
    ],
    'apps-pagetree-data-red-light' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Storages/apps-pagetree-data-red-light.svg',
    ],
    'apps-pagetree-data-red-neon' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Storages/apps-pagetree-data-red-neon.svg',
    ],
    'apps-pagetree-data-yellow' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Storages/apps-pagetree-data-yellow.svg',
    ],
    'apps-pagetree-data-yellow-light' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Storages/apps-pagetree-data-yellow-light.svg',
    ],
    'apps-pagetree-data-yellow-neon' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Storages/apps-pagetree-data-yellow-neon.svg',
    ],
    'apps-pagetree-data-article' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Storages/apps-pagetree-data-article.svg',
    ],
    'apps-pagetree-data-event' => [
        // Icon provider class
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        // The source SVG for the SvgIconProvider
        'source' => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/PageTreeIcons/Storages/apps-pagetree-data-event.svg',
    ],
];
