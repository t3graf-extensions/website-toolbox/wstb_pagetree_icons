<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/wstb_pagetree_icons.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\WstbPagetreeIcons\Controller;

/**
 * This file is part of the "Pagetree Icons" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 T3graf Development-Team <development@t3graf-media.de>
 */

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Backend\Routing\UriBuilder;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Http\JsonResponse;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException;
use TYPO3\CMS\Extbase\Mvc\Exception\StopActionException;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * PagetreeIconsController
 */
class PagetreeIconsToolController extends ActionController
{
    /**
     * action listIcons
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function listIconsAction(): ResponseInterface
    {
        $pageId = null;
        $urlvars = $_GET;
        $defaultPageIconClass = null;
        if (isset($urlvars['id']) || isset($urlvars['pageId'])) {
            $pageId = $urlvars['pageId'] ?? $urlvars['id'];
        }
        $page0 = true;

        if ($pageId !== '0' && $pageId !== null) {
            $page0 = false;
        }
        $langFile = 'LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf';

        $pageIconClass = '';

        //Get Page Icon
        $pageIconName = $this->getPageIcon($pageId);

        // Get Page Type
        $pageType = (string) $this->getPageType($pageId);

        // Get Page Menu State
        $pageMenuState = $this->getPageMenuState($pageId);

        // Get Page Hidden State
        $pageIsHidden = $this->isHidden($pageId);

        // Get Page Is Limited In Time
        $pageIsLimitedInTime = $this->isLimitedInTime($pageId);

        // Get Page Access Group
        $pageGroupAccess = $this->getPageGroupAccess($pageId);

        // Default page type label
        $pageTypeLabel = $langFile . ':doktype.I.0';

        switch($pageType) {
            // DOKTYPE_DEFAULT
            case '1':
                $pageTypeLabel = $langFile . ':doktype.I.0';
                break;

                // DOKTYPE_LINK
            case '3':
                $pageTypeLabel = 'LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.doktype.I.8';
                break;

                // DOKTYPE_SHORTCUT
            case '4':
                $pageTypeLabel = 'LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.doktype.I.2';
                break;

                // DOKTYPE_BE_USER_SECTION
            case '6':
                $pageTypeLabel = 'LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.doktype.I.4';
                break;

                // DOKTYPE_MOUNTPOINT
            case '7':
                $pageTypeLabel = 'LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.doktype.I.5';
                break;

                // DOKTYPE_SPACER
            case '199':
                $pageTypeLabel = 'LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.doktype.I.7';
                break;

                // DOKTYPE_SYSFOLDER
            case '254':
                $pageTypeLabel = $langFile . ':doktype.I.1';
                break;

                // DOKTYPE_RECYCLER
            case '255':
                $pageTypeLabel = $langFile . ':doktype.I.2';
                break;
        }

        if ($pageType !== '') {
            $defaultPageIconClass = $GLOBALS['TCA']['pages']['ctrl']['typeicon_classes'][$pageType];
            if ($this->isNavHide($pageId)) {
                $defaultPageIconClass = $GLOBALS['TCA']['pages']['ctrl']['typeicon_classes'][$pageType . '-hideinmenu'];
            }
            if ($this->isSiteRoot($pageId)) {
                $defaultPageIconClass = $GLOBALS['TCA']['pages']['ctrl']['typeicon_classes'][$pageType . '-root'];
            }
        }

        // Set default icon

        $default_label = LocalizationUtility::translate('defaultPageType', 'wstb_pagetree_icons');
        $icons[0] = [ $default_label, '', $defaultPageIconClass, 'svg', 'default'];

        $selectedIcon = '';
        $tabs = [];

        foreach ($GLOBALS['TCA']['pages']['columns']['module']['config']['items'] as $declared_icons) {
            // declared_icons
            // 0 > label
            // 1 > icon id
            // 2 > class
            // 3 > type
            // 4 > category

            if (empty($declared_icons[4])) {
                $declared_icons[4] = 'default';
            }

            if (empty($declared_icons[0])) {
                continue;
            }

            //Get Page icon class
            if (!empty($pageIconName)) {
                if ($declared_icons[1]===$pageIconName || strpos($pageIconName, $declared_icons[1]) === 0) {
                    $pageIconClass = $declared_icons[2];
                    $selectedIcon = $pageIconName;
                    $pIN = substr($pageIconName, -2);
                    if ($pIN === '-s' || $pIN === '-h') {
                        $selectedIcon = substr($pageIconName, 0, -2);
                    }

                    // Set tab
                    $tabs[$declared_icons[4]]['isactive'] = 'show active';
                }
            } else {
                $pageIconClass = $defaultPageIconClass;
                $tabs['default']['isactive'] = 'show active';
            }
            //Remove empty icons or Shortcut Icons
            $dis = substr($declared_icons[1], -2);
            if (empty($declared_icons[0]) || $dis === '-s' || $dis === '-h') {
                continue;
            }

            $declared_icons[3] = 'svg';

            if (strpos($declared_icons[0], 'LLL') === 0) {
                $declared_icons[0] = LocalizationUtility::translate($declared_icons[0], 'wstb_pagetree_icons');
            }
            $icons[] = $declared_icons;
        }

        $this->view->assignMultiple([
            'version'               => ExtensionManagementUtility::getExtensionVersion('wstb_pagetree_icons'),
            'pageTypeLabel'         => $pageTypeLabel,
            'icons'                 => $icons,
            'pageId'                => $pageId,
            'pageIcon'              => $pageIconClass,
            'selectedIcon'          => $selectedIcon,
            'page0'                 => $page0,
            'pageTitle'             => $this->getPageTitle($pageId),
            'tabs'                  => $tabs,
            'pageMenuState'         => $pageMenuState,
            'pageIsHidden'          => $pageIsHidden,
            'pageIsLimitedInTime'   => $pageIsLimitedInTime,
            'pageGroupAccess'       => $pageGroupAccess
        ]);
        return $this->htmlResponse();
    }

    /**
     * action changePageIcon
     *
     * @param  ServerRequestInterface $request
     * @return JsonResponse
     * @throws StopActionException|\Doctrine\DBAL\DBALException
     */
    public function changepageiconAction(ServerRequestInterface $request): JsonResponse
    {
        $newIcon = $request->getQueryParams()['newIcon'] ?? null;

        $pageId = $request->getQueryParams()['pageId'] ?? null;

        $pageType = $this->getPageType($pageId);
        // Force Shortcut Icon
        if (($pageType === '4' && strpos($newIcon, 'page') === 0) || ($pageType === '4' && strpos($newIcon, 'symbearth') === 0)) {
            $newIcon.= '-s';
        }

        // Force Hidden in menu Icon
        $pageMenuState = $this->getPageMenuState($pageId);
        if ($pageMenuState === '1') {
            $newIcon.= '-h';
        }

        //Update page icon in database
        $page_update = false;
        $pagesQueryBuilder =  GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\ConnectionPool::class)->getQueryBuilderForTable('pages');
        $page_update = $pagesQueryBuilder
            ->update('pages')
            ->where($pagesQueryBuilder->expr()->eq('uid', $pagesQueryBuilder->createNamedParameter($pageId, \PDO::PARAM_INT)))
            ->set('tstamp', time())
            ->set('module', $newIcon)
            ->execute();

        return new JsonResponse(
            [
                'success' => true,
            ]
        );
    }

    /**
     * action editPageProperties
     * @return void
     * @throws NoSuchArgumentException
     */
    public function editPagePropertiesAction()
    {
        $pageId = $this->request->getArgument('pageId');
        $urlEditPage = '';
        if (version_compare(TYPO3_version, '9.0', '<')) {
            $sReturnUrl = BackendUtility::getModuleUrl('web_RtPagesTreeIconsMod1', [
                'id' => $pageId
            ]);
            $urlEditPage = BackendUtility::getModuleUrl('record_edit', [
                'edit[pages][' . $pageId . ']' => 'edit',
                'pageId' => $pageId,
                'returnUrl' => $sReturnUrl
            ]);
        } elseif (version_compare(TYPO3_version, '9.0', '>=')) {
            $uriBuilder = GeneralUtility::makeInstance(UriBuilder::class);
            $sReturnUrl = (string)$uriBuilder->buildUriFromRoute('web_RtPagesTreeIconsMod1', [ 'id' => $pageId ]);
            $urlEditPage = (string)$uriBuilder->buildUriFromRoute('record_edit', [
                'edit[pages][' . $pageId . ']' => 'edit',
                'pageId' => $pageId,
                'returnUrl' => $sReturnUrl
            ]);
        }
        $this->response->setStatus(303);
        $this->response->setHeader('Location', $urlEditPage);
    }

    /**
     * utils get Page Menu State
     * @param $pageId
     * @return bool
     */
    protected function getPageMenuState($pageId)
    {
        $pageMenuState = null;

        $tce = GeneralUtility::makeInstance(DataHandler::class);
        if ($pageId !== '0' && $pageId !== null) {
            $pageMenuState = $tce->pageInfo($pageId, 'nav_hide');
        }
        return $pageMenuState;
    }

    /**
     * utils get Page Icon
     * @param $pageId
     * @return string
     */
    protected function getPageIcon($pageId)
    {
        $pageIcon = null;
        $tce = GeneralUtility::makeInstance(DataHandler::class);

        if ($pageId !== '0' && $pageId !== null) {
            $pageIcon = $tce->pageInfo($pageId, 'module');
        }
        return $pageIcon;
    }

    /**
     * utils get Page Icon
     * @param $pageId
     * @return string
     */
    protected function getPageType($pageId)
    {
        $pageType = null;
        $tce = GeneralUtility::makeInstance(DataHandler::class);

        if ($pageId !== '0' && $pageId !== null) {
            $pageType = $tce->pageInfo($pageId, 'doktype');
        }

        return $pageType;
    }

    /**
     * Utils Get Is Site Root
     * @param $pageId
     * @return bool
     */
    protected function isSiteRoot($pageId)
    {
        $isSiteRoot = null;
        $tce = GeneralUtility::makeInstance(DataHandler::class);
        if ($pageId !== '0' && $pageId !== null) {
            $isSiteRoot = $tce->pageInfo($pageId, 'is_siteroot');
        }
        return $isSiteRoot;
    }

    /**
     * Utils Get Is Hidden In Navigation
     * @param $pageId
     * @return bool
     */
    protected function isNavHide($pageId)
    {
        $isNavHide = null;
        $tce = GeneralUtility::makeInstance(DataHandler::class);
        if ($pageId !== '0' && $pageId !== null) {
            $isNavHide = $tce->pageInfo($pageId, 'nav_hide');
        }
        return $isNavHide;
    }

    /**
     * Utils Get Is Page Hidden
     * @param $pageId
     * @return bool
     */
    protected function isHidden($pageId)
    {
        $isHidden  = null;
        $tce = GeneralUtility::makeInstance(DataHandler::class);
        if ($pageId !== '0' && $pageId !== null) {
            $isHidden = $tce->pageInfo($pageId, 'hidden');
        }
        return $isHidden;
    }

    /**
     * Utils Get Is Limited In Time
     * @param $pageId
     * @return bool
     */
    protected function isLimitedInTime($pageId)
    {
        $isLimitedInTime = null;
        $tce = GeneralUtility::makeInstance(DataHandler::class);
        if ($pageId !== '0' && $pageId !== null) {
            $isLimitedInTime = $tce->pageInfo($pageId, 'starttime') > 0 || $tce->pageInfo($pageId, 'endtime') > 0;
        }
        return $isLimitedInTime;
    }

    /**
     * Utils Get Page Title
     * @param $pageId
     * @return string
     */
    protected function getPageTitle($pageId)
    {
        $getPageTitle = null;
        $tce = GeneralUtility::makeInstance(DataHandler::class);
        if ($pageId !== '0' && $pageId !== null) {
            $getPageTitle =  $tce->pageInfo($pageId, 'title');
        }
        return $getPageTitle;
    }

    /**
     * Utils Get Page Group Access
     * @param $pageId
     * @return int
     */
    protected function getPageGroupAccess($pageId)
    {
        $getPageGroupAccess = null;
        $tce = GeneralUtility::makeInstance(DataHandler::class);
        if ($pageId !== '0' && $pageId !== null) {
            $getPageGroupAccess = $tce->pageInfo($pageId, 'fe_group');
        }
        return $getPageGroupAccess;
    }
}
