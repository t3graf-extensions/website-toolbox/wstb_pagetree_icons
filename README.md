# TYPO3 Extension: `Page tree icons for website toolbox`
[![TYPO3 compatibility](https://img.shields.io/static/v1?label=TYPO3&message=v11%20%7C%20v12&color=f49800&&logo=typo3&logoColor=f49800)](https://typo3.org)
[![Packagist PHP Version Support (specify version)](https://img.shields.io/packagist/php-v/t3graf/wstb-pagetree-icons/dev-main?color=purple&logo=php&logoColor=white)](https://packagist.org/packages/t3graf/wstb-pagetree-icons)
[![Latest Stable Version](https://img.shields.io/packagist/v/t3graf/wstb-pagetree-icons?label=stable&color=33a2d8&logo=packagist&logoColor=white)](https://packagist.org/packages/t3graf/wstb-pagetree-icons)
[![Total Downloads](https://img.shields.io/packagist/dt/t3graf/wstb-pagetree-icons?color=1081c1&logo=packagist&logoColor=white)](https://packagist.org/packages/t3graf/wstb-pagetree-icons)
[![License](https://badgen.net/packagist/license/t3graf/wstb-pagetree-icons)](https://gitlab.com/typo3graf/developer-team/extensions/website_toolbox/-/blob/master/LICENSE.md)
[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/t3graf-extensions/website-toolbox/wstb_pagetree_icons?branch=main&label=CI%2FCD&logo=gitlab)](https://gitlab.com/t3graf-extensions/website_toolbox/-/commits/master)

> Page tree modul for website toolbox.

### Features

- Change page tree icons

|                  | URL                                            |
|------------------|------------------------------------------------|
| **Repository:**  | [gitlab.com/t3graf-extensions/website-toolbox/wstb_pagetree_icons](https://gitlab.com/t3graf-extensions/website-toolbox/wstb_pagetree_icons)                |
| **Documentation:** | LINK |
| **TER:**         | [extensions.typo3.org/](https://extensions.typo3.org/)    |

## Compatibility

| Website Toolbox     | TYPO3     | PHP       | Support / Development                |
|----------|-----------|-----------|--------------------------------------|
| dev-main | 11   | 8.0 - 8.1 | unstable development branch          |
| 1.x        | 11   | 8.0 - 8.1 | features, bugfixes, security updates |

## Need help?

* Read how to install, configure and use mask in the [official documentation](https://docs.typo3.org/p/mask/mask/main/en-us/)
* [Visit our website](https://www.t3graf-media.de) to find more information about website toolbox

## Found a bug?

* First check out the main branch and verify that the issue is not yet solved
* Have a look at the existing [issues](https://gitlab.com/t3graf-extensions/website-toolbox/wstb_pagetree_icons/issues), to prevent duplicates
* If not found, report the bug in our [issue tracker](https://gitlab.com/t3graf-extensions/website-toolbox/wstb_pagetree_icons/issues/new)

 **Please use Gitlab only for bug-reports or feature-requests.**

## Like a new feature?

* If your idea is not listed here, get in [contact](https://www.t3graf-media.de/) with us
* If you want to sponsor a feature, get in [contact](https://www.t3graf-media.de/) with us
* If you want to develop a feature, get in [contact](https://www.t3graf-media.de/) to plan a strategy

## Credits

This extension was created by Mike Tölle in 2022 for [T3graf media-agentur, Recklinghausen](https://www.t3graf-media.de).

Find examples, use cases and best practices for this extension in our [website_toolbox blog series on t3graf.de](https://www.t3graf-media.de/blog/).

[Find more TYPO3 extensions we have developed](https://www.t3graf-media.de/) that help us deliver value in client projects. As part of the way we work, we focus on testing and best practices to ensure long-term performance, reliability, and results in all our code.

Big thanks to
==================

Regis TEDONE and the the `rt_pages_tree_icons` extension that has served as 100% inspiration and reuse of code

https://github.com/Treedent/rt_pages_tree_icons
