/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2021 Regis TEDONE <regis.tedone@gmail.com>, SYRADEV
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

define('T3graf/PagesTreeIcons/iconChanger', ['jquery'], function() {
    $('li.icon-container').on('click', function() {
        $(this).siblings().removeClass('icon-current');
        $(this).addClass('icon-current');
        $('#newIcon').val($(this).data('icon'));
    });
});
require([
    'jquery',
    'TYPO3/CMS/Core/Ajax/AjaxRequest'
], function ($,AjaxRequest) {
    $('.save-page-tree-icon').click(function (event) {
        const newIcon = $(this).data('icon');
        const pageId = $(this).data('page-id')
        new AjaxRequest(TYPO3.settings.ajaxUrls.page_tree_icons_change_page_icon)
            .withQueryArguments({newIcon:newIcon,pageId:pageId})
            .get()
            .then(async function (response) {
                top.document.dispatchEvent(new CustomEvent("typo3:pagetree:refresh"));
                window.location.reload();
            });
    });
});
