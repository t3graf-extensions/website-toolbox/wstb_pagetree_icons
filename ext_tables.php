<?php

/*
 * This file is part of the package t3graf/wstb_pagetree_icons.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3') || die();

(static function () {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'WstbPagetreeIcons',
        'WebsiteToolboxWebsitetools',
        'pagetreeiconstool',
        'before:WebsiteToolboxSitepackagetool',
        [
            \T3graf\WstbPagetreeIcons\Controller\PagetreeIconsToolController::class => 'listIcons, changepageicon',

        ],
        [
            'access' => 'user,group',
            'icon'   => 'EXT:wstb_pagetree_icons/Resources/Public/Icons/module-pagetreeicon.svg',
            'labels' => 'LLL:EXT:wstb_pagetree_icons/Resources/Private/Language/locallang_pagetreeicons.xlf',
            'navigationComponentId' => 'TYPO3/CMS/Backend/PageTree/PageTreeElement',
        ]
    );

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_wstbpagetreeicons_domain_model_pagetreeicons', 'EXT:wstb_pagetree_icons/Resources/Private/Language/locallang_csh_tx_wstbpagetreeicons_domain_model_pagetreeicons.xlf');
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_wstbpagetreeicons_domain_model_pagetreeicons');
})();
